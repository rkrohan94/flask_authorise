from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired


class CreateCategoryForm(FlaskForm):
    category_name = StringField('category_name', id='category_name', validators=[DataRequired()])


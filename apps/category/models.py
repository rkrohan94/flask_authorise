from apps import db
import datetime
from flask_authorize import PermissionsMixin


class Category(db.Model, PermissionsMixin):
    __tablename__ = 'categories'

    __permissions__ = dict(
        owner=['read', 'update', 'delete', 'revoke'],
        group=['read', 'update'],
        other=['read']
    )

    id = db.Column(db.Integer, primary_key=True)
    category_name = db.Column(db.String(64))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    updated_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
   
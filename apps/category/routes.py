from apps.category import blueprint
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_required
from apps.category.models import Category
from apps.category.forms import CreateCategoryForm

from apps import db, authorize
from werkzeug.exceptions import Unauthorized


@blueprint.route('/category')
@login_required
@authorize.read
def index():
    category = Category.query.all()
    return render_template('category/index.html', category=category)


@blueprint.route('/category/create', methods=['GET', 'POST'])
@login_required
def create():
    if not authorize.create(Category):
        raise Unauthorized
    category_form = CreateCategoryForm(request.form)
    if 'create' in request.form:
        category_name = request.form['category_name']
        category = Category(
            category_name=category_name
        )
        db.session.add(category)
        db.session.commit()
        return redirect(url_for('category_blueprint.index'))
    return render_template('category/create.html', form=category_form)


@blueprint.route('/category/update/<int:category_id>', methods=['GET', 'POST'])
@authorize.update
@login_required
def update(category_id):
    category = Category.query.filter_by(id=category_id).first()
    category_form = CreateCategoryForm(formdata=request.form, obj=category)
    if not authorize.update(category):
        raise Unauthorized
    if 'update' in request.form:
        category_form.populate_obj(category)
        db.session.commit()
        return redirect(url_for('category_blueprint.index'))
    return render_template('category/edit.html', form=category_form)


@blueprint.route('/category/delete/<int:category_id>', methods=['GET', 'POST'])
@authorize.delete
@login_required
def delete(category_id):
    category = Category.query.filter_by(id=category_id).first()
    if not authorize.delete(category):
        raise Unauthorized
    if category:
        db.session.delete(category)
        db.session.commit()
        return redirect(url_for('category_blueprint.index'))
    return redirect(url_for('category_blueprint.index'))


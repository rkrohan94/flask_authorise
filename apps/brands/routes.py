from apps.brands import blueprint
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_required
from apps.brands.models import Brands
from apps.brands.forms import CreateBrandForm

from apps import db, authorize
from werkzeug.exceptions import Unauthorized, NotFound


@blueprint.route('/brands', methods=['GET'])
@authorize.read
@login_required
def index():
    brands = Brands.query.all()
    return render_template('brands/index.html', brands=brands)


@blueprint.route('/brands/create', methods=['GET', 'POST'])
@login_required
def create():
    if not authorize.create(Brands):
        raise Unauthorized
    brand_form = CreateBrandForm(request.form)
    if 'create' in request.form:
        brand_name = request.form['brand_name']
        brands = Brands(
            brand_name=brand_name
        )
        db.session.add(brands)
        db.session.commit()
        return redirect(url_for('brands_blueprint.index'))
    return render_template('brands/create.html', form=brand_form)


@blueprint.route('/brands/update/<int:brand_id>', methods=['GET', 'POST'])
@authorize.update
@login_required
def update(brand_id):
    brand = Brands.query.filter_by(id=brand_id).first()
    brand_form = CreateBrandForm(formdata=request.form, obj=brand)
    if not authorize.update(brand):
        raise Unauthorized
    if 'update' in request.form:
        brand_form.populate_obj(brand)
        db.session.commit()
        return redirect(url_for('brands_blueprint.index'))
    return render_template('brands/edit.html', form=brand_form)


@blueprint.route('/brands/delete/<int:brand_id>', methods=['GET', 'POST'])
@authorize.delete
@login_required
def delete(brand_id):
    brand = Brands.query.filter_by(id=brand_id).first()
    if not authorize.delete(brand):
        raise Unauthorized
    if brand:
        db.session.delete(brand)
        db.session.commit()
        return redirect(url_for('brands_blueprint.index'))
    return redirect(url_for('brands_blueprint.index'))


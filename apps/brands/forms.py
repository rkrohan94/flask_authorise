from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired


class CreateBrandForm(FlaskForm):
    brand_name = StringField('brand_name', id='brand_name', validators=[DataRequired()])


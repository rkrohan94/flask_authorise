from flask import Blueprint

blueprint = Blueprint(
    'units_blueprint',
    __name__,
    url_prefix=''
)

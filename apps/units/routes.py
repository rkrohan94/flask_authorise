from apps.units import blueprint
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_required
from apps.units.models import Units
from apps.units.forms import CreateUnitForm

from apps import db, authorize
from werkzeug.exceptions import Unauthorized


@blueprint.route('/units')
@login_required
@authorize.read
def index():
    units = Units.query.all()
    print(units)
    return render_template('units/index.html', units=units)


@blueprint.route('/units/create', methods=['GET', 'POST'])
@login_required
@authorize.create(Units)
def create():
    unit_form = CreateUnitForm(request.form)
    if 'create' in request.form:
        unit_name = request.form['unit_name']
        units = Units(
            unit_name=unit_name
        )
        db.session.add(units)
        db.session.commit()
        return redirect(url_for('units_blueprint.index'))
    return render_template('units/create.html', form=unit_form)


@blueprint.route('/units/update/<int:unit_id>', methods=['GET', 'POST'])
@login_required
def update(unit_id):
    unit = Units.query.filter_by(id=unit_id).first()
    unit_form = CreateUnitForm(formdata=request.form, obj=unit)
    if not authorize.update(unit):
        raise Unauthorized
    if 'update' in request.form:
        unit_form.populate_obj(unit)
        db.session.commit()
        return redirect(url_for('units_blueprint.index'))
    return render_template('units/edit.html', form=unit_form)


@blueprint.route('/units/delete/<int:unit_id>', methods=['GET', 'POST'])
@login_required
def delete(unit_id):
    unit = Units.query.filter_by(id=unit_id).first()
    if not authorize.delete(unit):
        raise Unauthorized
    if unit:
        db.session.delete(unit)
        db.session.commit()
        return redirect(url_for('units_blueprint.index'))
    return redirect(url_for('units_blueprint.index'))


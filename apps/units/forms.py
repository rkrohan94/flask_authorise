from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired


class CreateUnitForm(FlaskForm):
    unit_name = StringField('unit_name', id='unit_name', validators=[DataRequired()])


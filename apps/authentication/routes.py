from flask import render_template, redirect, request, url_for
from sqlalchemy import text
from flask_login import (
    current_user,
    login_user,
    logout_user
)
# from flask_dance.contrib.github import github

from apps import db, login_manager, authorize
from apps.authentication import blueprint
from apps.authentication.forms import LoginForm, CreateAccountForm
from apps.authentication.models import User, Role, UserRole

from werkzeug.exceptions import Unauthorized

from apps.authentication.util import verify_pass
import hashlib


@blueprint.route('/')
def route_default():
    return redirect(url_for('authentication_blueprint.login'))


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    if 'login' in request.form:

        # read form data
        user_id = request.form['username']  # we can have here username OR email
        password = request.form['password']

        # Locate user
        user = User.query.filter_by(username=user_id).first()

        print(user)

        # if user not found
        if not user:

            user = User.query.filter_by(email=user_id).first()

            if not user:
                return render_template('accounts/login.html',
                                       msg='Unknown User or Email',
                                       form=login_form)

        # Check the password
        # if verify_pass(password, user.password):
        #     login_user(user)
        # return redirect(url_for('authentication_blueprint.route_default'))

        if user:
            hashed_password = hashlib.md5(password.encode())
            hashed_password = hashed_password.hexdigest()

            print(user.password)
            # print(hashed_password)

            if hashed_password == user.password:
                login_user(user)
                return redirect(url_for('home_blueprint.index'))

        # Something (user or pass) is not ok
        return render_template('accounts/login.html',
                               msg='Wrong user or password',
                               form=login_form)

    if not current_user.is_authenticated:
        return render_template('accounts/login.html',
                               form=login_form)
    return redirect(url_for('home_blueprint.index'))


@blueprint.route('/register', methods=['GET', 'POST'])
def register():
    create_account_form = CreateAccountForm(request.form)
    if 'register' in request.form:

        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        hashed_password = hashlib.md5(password.encode())
        hashed_password = hashed_password.hexdigest()

        # Check usename exists
        user = User.query.filter_by(username=username).first()
        if user:
            return render_template('accounts/register.html',
                                   msg='Username already registered',
                                   success=False,
                                   form=create_account_form)

        # Check email exists
        user = User.query.filter_by(email=email).first()
        if user:
            return render_template('accounts/register.html',
                                   msg='Email already registered',
                                   success=False,
                                   form=create_account_form)

        # else we can create the user
        # user = Users(**request.form)
        user = User(
            username=username,
            email=email,
            password=hashed_password
        )
        db.session.add(user)
        db.session.commit()

        # Delete user from session
        logout_user()

        return render_template('accounts/register.html',
                               msg='User created successfully.',
                               success=True,
                               form=create_account_form)

    else:
        return render_template('accounts/register.html', form=create_account_form)


@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('authentication_blueprint.login'))


@blueprint.route('/user')
def user():
    if not authorize.read(User):
        raise Unauthorized
    # users = User.query.join(UserRole, UserRole.user_id == User.id).all()
    t = text(
        "SELECT users.id as user_id, users.firstname, users.lastname, users.phone, "
        "users.address,users.username,users.email, user_role.*, roles.* "
        "FROM `users` "
        "JOIN user_role ON users.id = user_role.user_id "
        "JOIN roles  ON roles.id = user_role.role_id "
    )
    users = db.session.execute(t)

    return render_template('users/index.html', users=users)


@blueprint.route('/user/create', methods=['GET', 'POST'])
def create():
    create_account_form = CreateAccountForm(request.form)
    # create_account_form.unit_id.choices = [(units.id, units.unit_name) for units in Units.query.all()]
    if not authorize.create(User):
        raise Unauthorized
    if 'create' in request.form:
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        phone = request.form['phone']
        address = request.form['address']
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        confirm_password = request.form['confirm_password']

        if password == confirm_password:
            hashed_password = hashlib.md5(password.encode())
            hashed_password = hashed_password.hexdigest()
        else:
            return render_template('users/create.html',
                                   msg='Password Doesnt Match',
                                   success=False,
                                   form=create_account_form)

        # Check username exists
        user = User.query.filter_by(username=username).first()
        if user:
            return render_template('users/create.html',
                                   msg='Username already have',
                                   success=False,
                                   form=create_account_form)

        # Check email exists
        user = User.query.filter_by(email=email).first()
        if user:
            return render_template('users/create.html',
                                   msg='Email already have',
                                   success=False,
                                   form=create_account_form)

        # else we can create the user
        # user = Users(**request.form)
        user = User(
            firstname=firstname,
            lastname=lastname,
            phone=phone,
            address=address,
            username=username,
            email=email,
            password=hashed_password
        )
        db.session.add(user)
        db.session.commit()

        # Delete user from session
        logout_user()

        return redirect(url_for('authentication_blueprint.user',
                                msg='User created successfully.',
                                success=True,
                                form=create_account_form))

    else:
        return render_template('users/create.html', form=create_account_form)


@blueprint.route('/user/update/<int:user_id>', methods=['GET', 'POST'])
def update(user_id):
    user = User.query.filter_by(id=user_id).first()
    create_account_form = CreateAccountForm(formdata=request.form, obj=user)
    if not authorize.update(user):
        raise Unauthorized
    if 'update' in request.form:
        create_account_form.populate_obj(user)
        db.session.commit()
        return redirect(url_for('authentication_blueprint.user'))
    return render_template('users/edit.html', form=create_account_form)


# Errors

@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template('home/page-403.html'), 403


@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('home/page-403.html'), 403


@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('home/page-404.html'), 404


@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('home/page-500.html'), 500

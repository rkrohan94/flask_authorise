from flask_login import UserMixin

from sqlalchemy.orm import relationship
# from flask_dance.consumer.storage.sqla import OAuthConsumerMixin
from flask_authorize import RestrictionsMixin, AllowancesMixin
from flask_authorize import PermissionsMixin

from apps import db, login_manager

from apps.authentication.util import hash_pass

# mapping tables
UserGroup = db.Table(
    'user_group', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('group_id', db.Integer, db.ForeignKey('groups.id'))
)

UserRole = db.Table(
    'user_role', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('roles.id'))
)


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(64))
    lastname = db.Column(db.String(64))
    phone = db.Column(db.String(64))
    address = db.Column(db.String(64))
    username = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(100))

    # `roles` and `groups` are reserved words that *must* be defined
    # on the `User` model to use group- or role-based authorization.
    roles = db.relationship('Role', secondary=UserRole)
    groups = db.relationship('Group', secondary=UserGroup)

    def __repr__(self):
        return str(self.username)

    @classmethod
    def find_by_email(cls, email: str) -> "User":
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_username(cls, username: str) -> "User":
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id: int) -> "User":
        return cls.query.filter_by(id=_id).first()

    # def save(self) -> None:
    #     try:
    #         db.session.add(self)
    #         db.session.commit()
    #
    #     except SQLAlchemyError as e:
    #         db.session.rollback()
    #         db.session.close()
    #         error = str(e.__dict__['orig'])
    #         raise InvalidUsage(error, 422)
    #
    # def delete_from_db(self) -> None:
    #     try:
    #         db.session.delete(self)
    #         db.session.commit()
    #     except SQLAlchemyError as e:
    #         db.session.rollback()
    #         db.session.close()
    #         error = str(e.__dict__['orig'])
    #         raise InvalidUsage(error, 422)
    #     return


class Group(db.Model, RestrictionsMixin):
    __tablename__ = 'groups'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)


class Role(db.Model, AllowancesMixin, RestrictionsMixin):
    __tablename__ = 'roles'
    __restrictions__ = '*'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)

@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()


@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None

# class OAuth(OAuthConsumerMixin, db.Model):
#     user_id = db.Column(db.Integer, db.ForeignKey("users.id", ondelete="cascade"), nullable=False)
#     user = db.relationship(Users)

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import Email, DataRequired


# login and registration


class LoginForm(FlaskForm):
    username = StringField('Username',
                           id='username_login',
                           validators=[DataRequired()])
    password = PasswordField('Password',
                             id='pwd_login',
                             validators=[DataRequired()])


class CreateAccountForm(FlaskForm):
    firstname = StringField('First Name',
                           id='firstname_create')
    lastname = StringField('Last Name',
                           id='lastname_create')
    phone = StringField('Phone',
                           id='phone_create')
    address = StringField('Address',
                           id='address_create')
    username = StringField('Username',
                           id='username_create',
                           validators=[DataRequired()])
    email = StringField('Email',
                        id='email_create',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password',
                             id='pwd_create',
                             validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                             id='cpwd_create',
                             validators=[DataRequired()])

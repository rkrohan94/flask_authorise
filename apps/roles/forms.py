from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, TextAreaField, HiddenField
from wtforms.validators import DataRequired


class CreateRoleForm(FlaskForm):
    name = StringField('name', id='name', validators=[DataRequired()])
    allowances = HiddenField('allowances', id='allowances')
    restrictions = StringField('restrictions', id='restrictions')


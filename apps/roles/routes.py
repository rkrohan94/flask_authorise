import json, sys
from apps.roles import blueprint
from flask import render_template, redirect, request, url_for, flash, jsonify
from flask_login import login_required
from apps.authentication.models import Role
from apps.roles.forms import CreateRoleForm

from apps import db, authorize
from werkzeug.exceptions import Unauthorized, NotFound


@blueprint.route('/roles', methods=['GET'])
@login_required
def index():
    roles = Role.query.all()
    if not authorize.create(Role):
        raise Unauthorized
    return render_template('roles/index.html', roles=roles)


@blueprint.route('/roles/create', methods=['GET', 'POST'])
@login_required
def create():
    if not authorize.create(Role):
        raise Unauthorized
    role_form = CreateRoleForm(request.form)
    if 'create' in request.form:
        name = request.form['name']
        roles = Role(
            name=name
        )
        db.session.add(roles)
        db.session.commit()
        return redirect(url_for('roles_blueprint.index'))
    return render_template('roles/create.html', form=role_form)


@blueprint.route('/roles/update/<int:role_id>', methods=['GET', 'POST'])
@authorize.update
@login_required
def update(role_id):
    role = Role.query.filter_by(id=role_id).first()
    role_list = role.allowances

    role_form = CreateRoleForm(formdata=request.form, obj=role)
    if not authorize.update(role):
        raise Unauthorized
    if 'update' in request.form:
        role_name = request.form['name']
        allowances = request.form['allowances']

        json_object = json.loads(allowances)

        role.name = role_name
        role.allowances = json_object
        db.session.commit()

        return redirect(url_for('roles_blueprint.index'))
    return render_template('roles/edit.html', form=role_form, role=role, role_list=role_list)
#
#
# @blueprint.route('/brands/delete/<int:brand_id>', methods=['GET', 'POST'])
# @authorize.delete
# @login_required
# def delete(brand_id):
#     brand = Brands.query.filter_by(id=brand_id).first()
#     if not authorize.delete(brand):
#         raise Unauthorized
#     if brand:
#         db.session.delete(brand)
#         db.session.commit()
#         return redirect(url_for('brands_blueprint.index'))
#     return redirect(url_for('brands_blueprint.index'))

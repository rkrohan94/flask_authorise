from flask import Blueprint

blueprint = Blueprint(
    'roles_blueprint',
    __name__,
    url_prefix=''
)

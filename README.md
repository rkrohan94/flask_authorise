# Flask-Authorize

Flask-Authorize is a Flask extension designed for Role-Based Access Control (RBAC).

For more details [Click here](https://flask-authorize.readthedocs.io/en/latest/)

## Installation

First clone git repository
```bash
git clone git@gitlab.com:rkrohan94/flask_authorise.git
```
Go to project folder
```bash
cd flask_authorise
```
Create an virtual environment
```bash
python3 -m venv venv
```
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all package.

```bash
pip install -r requirements.txt
```

## Configure you Database on .env file

```python
    DB_ENGINE=mysql
    DB_NAME=flask_authorise
    DB_HOST=localhost
    DB_PORT=3306
    DB_USERNAME=root
    DB_PASS=

```
Initialize and migrate Database
```bash
flask db init
```
```bash
flask db migrate
```
Run Application
```bash
flask run
```
## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Screenshots

![App Screenshot](https://imgtr.ee/images/2023/05/09/ln631.png)

![App Screenshot](https://imgtr.ee/images/2023/05/09/lnN0X.png)

